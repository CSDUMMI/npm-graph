import time
import peewee as pw
import requests
import tempfile
import argparse
import json
from pprint import pprint
from pyvis.network import Network
import re
from urllib.parse import unquote
from datetime import datetime

database = pw.SqliteDatabase("deps.db")

class BaseModel(pw.Model):
    class Meta:
        database = database
        

class Package(BaseModel):
    name = pw.TextField(primary_key=True)
    added_already = pw.BooleanField(default=False)

class Dependency(BaseModel):
    dependent_package = pw.ForeignKeyField(Package, backref="dependencies")
    dependency = pw.ForeignKeyField(Package, backref="depended_on")

    def __repr__(self):
        return f"<Dependency {self.id} {self.dependent_package.name}<-{self.dependency.name}>"
    
def to_name(name_and_version):
    return name_and_version.rsplit("@",maxsplit=1)[0]

def to_version(name_and_version):
    return name_and_version.rsplit("@",maxsplit=1)[1]
    
def add_cli(argv):
    """
    """
    name = argv.name
    deps_before = Dependency.select().count()
    pkgs_before = Package.select().count()

    add(name, max_packages=argv.max_packages)
        
    deps_after = Dependency.select().count()
    pkgs_after = Package.select().count()
    print(f"Added {deps_after-deps_before} dependency relationships.\nAdded {pkgs_after-pkgs_before} packages")

def fetch_dependencies_of(name):
    return fetch_from_npm(name, "dependencies")

def fetch_dependents_of(name):
    return fetch_from_npm(name, "dependents")

def fetch_related_of(name):
    pkgs, relations = fetch_dependencies_of(name)
    pkgs_, relations_ = fetch_dependents_of(name)
    return pkgs | pkgs_, relations | relations_

def fetch_from_npm(name, activeTab):
    response = requests.get(f"https://www.npmjs.com/package/{name}?activeTab={activeTab}")

    i = 0
    while response.status_code != requests.codes.ok:
        backoff = 30*2**i
        retry_after = response.headers.get("Retry-After")

        if retry_after is not None:
            backoff = int(retry_after)
            
        print(f"Backing off for {backoff} seconds. NPM responded with {response.status_code}")
        time.sleep(backoff)
        i += 1
        response = requests.get(f"https://www.npmjs.com/package/{name}?activeTab={activeTab}")

    html = response.text

    pattern = re.compile(r"<a href=\"\/package\/(\S+)\"")

    pkgs = set()
    relations = set()
    for match in pattern.finditer(html):
        rel_name = unquote(match.group(1))
        pkgs.add(rel_name)

        if activeTab == "dependencies":
            relations.add((name, rel_name))
        else:
            relations.add((rel_name, name))

    time.sleep(0.5)
    return pkgs, relations

def add(name, max_packages=-1):
    package, created = Package.get_or_create(name=name)

    unprocessed, relations = fetch_related_of(name)
    print(f"Processing {unprocessed}")
    while len(unprocessed) > 0 and max_packages != 0:
        related_name = unprocessed.pop()
        print(f"Adding {related_name}")
        related_pkg, created = Package.get_or_create(name=related_name)

        if created:
            max_packages -= 1
            new_names, relations_ = fetch_related_of(related_name)
            unprocessed |= new_names
            relations |= relations_
            print("Unprocessed left:", len(unprocessed))
            print("Relations: ", len(relations))
            if max_packages >= 0:
                print(f"Packages left: {max_packages}") 
            
    for (dependent, dependency) in relations:
        Dependency.create(
            dependent_package=Package.get_or_create(name=dependent)[0],
            dependency=Package.get_or_create(name=dependency)[0])
    
    print("Added " + package.name)
    return True
    
def query(args):
    result = []
    for package in Package.select().join(Dependency):
        result.append((package, Dependency.select().where(Dependency.dependent_package == package).count()))

    for package, dependent in sorted(result, key=lambda x: x[1]):
        print(package, dependent)
    
def visualise(args):
    net = Network(select_menu=True)

    for pkg in Package.select():
        net.add_node(pkg.name, label=pkg.name, title=pkg.name)

    for dep in Dependency.select():
        net.add_edge(dep.dependent_package.name, dep.dependency.name)

    net.show_buttons(filter_=True)
    net.show(args.output_file, notebook=False, local=False)

    cleanup_html(args.output_file)

def cleanup_html(output_file):
    with open(args.output_file) as fh:
        html = fh.read().replace("https://cdnjs.cloudflare.com/ajax/libs/vis-network/9.1.2/dist/dist/vis-network.min.css", "/lib/vis-network.min.css")
        html = html.replace("https://cdnjs.cloudflare.com/ajax/libs/vis-network/9.1.2/dist/vis-network.min.js", "/lib/vis-network.min.js")
        html = html.replace("https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css", "/lib/bootstrap.min.css")
        

def add_from_md_file(args):
    with open(args.input_file) as fh:
        lines = fh.read().split("\n")

        pattern = re.compile(r"\d+\. \[([a-zA-Z0-9]+)\]\(.+\) - \d+")
        for line in lines:
            if match := pattern.match(line):
                name = match.group(1)
                add(name)
            
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="Dependency Inserter")
    parser.add_argument("--init-db", action=argparse.BooleanOptionalAction)
    subparsers = parser.add_subparsers()
    
    add_parser = subparsers.add_parser("add", help="Add a new package to the graph")
    add_parser.add_argument("name", type=str)
    add_parser.add_argument("--max-packages", type=int, default=-1)
    add_parser.set_defaults(func=add_cli)

    query_parser = subparsers.add_parser("query", help="Query the dependencies")
    query_parser.set_defaults(func=query)

    visualise_parser = subparsers.add_parser("visualise", help="Visualise the network")
    visualise_parser.add_argument("output_file", help="Path of the output HTML file containing the network")
    visualise_parser.set_defaults(func=visualise)

    import_parser = subparsers.add_parser("import", help="Import list of packages from MD file")
    import_parser.add_argument("input_file", help="Input Markdown File")
    import_parser.set_defaults(func=add_from_md_file)
    
    args = parser.parse_args()
    
    database.connect()
    if args.init_db:
        database.create_tables([Package, Dependency])
    args.func(args)
    database.close()

# Create a graph of dependencies between NPM packages.

1. Create a new npm project
2. Install a single dependency
3. Run `npm sbom` command 
4. Add dependencies to graph
5. Show ends of the graph - points used by a lot of popular dependencies but not used a lot themselves.
